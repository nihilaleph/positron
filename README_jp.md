# Positron
Current stable version: **1.1.0**


Positronとは、LudumDare 35のエントリー（ShapeMagnet）を基づいて進行中のプロジェクトです。

引力と斥力のある混沌とした空間をナビゲートして中性への脱出を見つけましよう。

**Links**
- README English version: [README.md](README.md)
- Itch.ioストアー: https://nihilaleph.itch.io/positron
- Gitlabリポジトリ: https://gitlab.com/nihilaleph/positron
- 企画書：https://docs.google.com/presentation/d/1TnRb9mmyRT5vyNshshot34NmcEd-uYazP40jCUkIkRk/edit?usp=sharing

**目次**

[[_TOC_]]

## 概要
このゲームは物理的なアクションパズルです。プレイヤーがポジティブとネガティブの形で変身能力を持って、その形に応じて、他のエージェントを引力させるか斥力させるか決まります。スタイルは、Geometry Warsなどの電子的でネオンビジュアル、そしてPortalやゼルダの伝説などの物理的なパズルゲームに触発されています。
## メカニックス

### エージェント（Agent）
このゲームのエージェントはプレイヤーに対して積極的に攻撃することではありません。プレイヤーを含めて、全てのエージェントは**ポジティブ**または**ネガティブ**の**電荷（charge）**があります。エージェントは力積で動いて、力積に応じて回転します。また、エージェントは死亡することがあって、それによって様々なエフェクトが生成します。

![positron-class](https://drive.google.com/uc?id=1J394WnGvU65bp760eITv9rC4UzvzndVk)

*図2.1：全エージェントのUMLクラス図*

#### プレイヤー（Player）
プレイヤーは、電荷を変化できる本体を操作します。アナログスティックやキーボードの矢印キーで移動し、指定されたアクションボタン（A、X、スペースバー...）で変化を行うことができます。

![positro-player](https://lh4.googleusercontent.com/Cc6igp5rQgSMwgBTrocNJfOomBrFBWnHrdOyv3QsIR4ml3FT733ud5Jy2cXAA4_Rxnckcc0t5Z0f6QwLgP9gOAiqXMRBZZtDm3W3bzsisUHMalTjs5_LBWKWZ4ic9Tz2L89z2dS-)

*図2.2：プレーヤーのネガティブ形とポジティブ形*

#### 敵（Enemies）
敵の動きは電気力みたいにプレイヤーに対する*電荷*と*位置*によります。 電荷が同じと斥力されて、異なると引力されることです。この相互作用は、エージェントがプレイヤーに近いほど強くなります。互いにぶつかると相互作用して、壁にぶつかると跳ね返ります。

 - Simple Enemy: 異なる電荷またはプレイヤーと1回ぶつかると死亡

![positron-enemy](https://lh5.googleusercontent.com/dmIkjtT0OW6J3epwGpHovBwN2kqKw0uoZjwp14eTXFztdfNbNqC19Yd5ozU8mUh9E6ltjxmkMMpzYUJcQi7ZGfCA2FrbtfGdlWKxy3foS4n_pPCrih9ATfDXdTHwOwVseDmhTFg4)

*図2.3：Simple Enemyのネガティブ形とポジティブ形*

 - Strong Enemy: 異なる電荷またはプレイヤーと3回ぶつかると死亡
 
![positron-strong](https://drive.google.com/uc?id=18dvV0-9NHnz1RP0my8xeu0XDO3YHDZ6g)

*図2.4：Strong Enemyのネガティブ形とポジティブ形*

 - Block Enemy: 死亡せず、多くの相互作用ポイントがある
 
![positron-block](https://drive.google.com/uc?id=1CvFqmdDa5qFGxyyNOK4Gy-mYeD3sQHce)

*図2.5：Block Enemyのネガティブ形とポジティブ形*

 - Unstoppable Enemy: 死亡せず、減速せず

![positron-unstoppable](https://drive.google.com/uc?id=1J5uQS-LJDJGat2i1u0PDhy25dtCoRPek)

*図2.6：Unstoppable Enemyのネガティブ形とポジティブ形*
 
 - Blackhole Enemy: すべてのエージェントと相互作用するオーラ、中心にあるエージェントを破壊する
 
![positron-blackhole](https://drive.google.com/uc?id=1nqaXC-IVSNPNXBgv3DLDAW1390HM_U1g)

*図2.7：Blackhole Enemyのネガティブ形とポジティブ形*


## レベルデザイン
プレイヤーの目的は、壁や他のエージェントをぶつけることなく、次のレベルにつながる出口に到達することです。プレイヤーがぶつけると死亡して、レベルを再起動するオプションがあります。多くのレベルでは、プレイヤーが出口を開くためにキーを取得する必要があります。

![positron-annotation](https://drive.google.com/uc?id=1dfhiwaW9-6J7Dw1nlLwuMjjOQCAwaFFQ)

*図3.1：オブジェクトとイ相互作用の注釈付きスクリーンショット*

**ワールド**は4つがあって、各ワールドには**レベル**10つがあります。ワールド内の前半のレベルはプレイヤーに新しいアイデアを提示して、後半のレベルは挑戦を深めます。チュートリアルや説明はないので、プレイヤーは自由に探検して自分なりの方法を見つけてレベルをクリアしていきます。

プレイヤーがワールドを完了すると、記録時間が保存され、次のワールドに進むオプションがあります。

すべてのレベルは、少なくとも3つのキー、出口、プレイヤー、パララックス背景、壁を確定するタイルマップ、ゲームコントローラとコントロールメニューで構成されています。

![positron-screenshot](https://img.itch.zone/aW1hZ2UvNDQyMzE3LzI0OTI3NzYuZ2lm/original/KAdpoD.gif)

*図 3.2：ワールド 4 レベル 8 の例（速度 x2.0）*

 - ワールド0: Simple Enemiesとキー
 - ワールド1: Block EnemiesとStrong Enemies
 - ワールド2: Blackhole EnemiesとUnstoppable Enemies
 - ワールド3: 最後の挑戦

## インターフェイス
### HUD
タイトルメニューには、ワールド選択メニューと最高記録時間、操作方法の指示、そしてゲーム終了のオプションが表示されます。ゲーム内メニューには、ポーズメニュー、死後メニュー、ワールド終了時の記録入力メニューがあります。

![positron-title-menu](https://drive.google.com/uc?id=1bMsMDe5chgAJREkRXaEiVlSIGf9GHnhs)

*図4.1：タイトルメニュー*

![positron-control](https://drive.google.com/uc?id=1S6Aw2utB5X5qppx-4MrgGSDm564lTHro)

*図4.2：操作方法メニュー*

![positron-world-menu](https://drive.google.com/uc?id=1kmKxDCDrbCxYpuiJoMOHrlxHptuIMxKi)

*図4.3：ワールド選択メニュー*

ゲームの状態からプレイヤーの気を散らさないように、ゲームプレイ中に目に見えるHUDがありません。

プレイヤーはいつでもゲームをポーズすることができます。メニューに、再起動、レベルの再挑戦、タイトルメニューに戻るなどのオプションが表示されます。

![positron-pause-menu](https://drive.google.com/uc?id=16yzX3pVgiIklOiqlvYcuYCniYJMUF61K)

*図4.4：ポーズメニュー*

プレイヤーが死亡すると、レベルを再起動、タイトルメニューに戻るのオプションのメニューが表示されます。

![positron-post-death-menu](https://drive.google.com/uc?id=1MVyzKU79tZUP-_vkXsagnxHeJASPfliq)

*図4.5：死後メニュー*

ワールドをクリアすると、クリアまでにかかった時間が表示されて、3文字を入力して記録を保存することができます。メニューにはベストタイムと現在記録時間が表示されて、次のワールドに行くか、タイトルメニューに戻るかを選択できます。

![positron-record](https://drive.google.com/uc?id=1sij8LVyw_CjKEWM-7BWq0YpQln2NNLCK)

*図4.6：記録入力メニュー*

![positron-record-menu](https://drive.google.com/uc?id=1QMJY9JO7L3it0lH0kX2jrVrsp_noJ2qc)

*図4.7：次のワールドメニュー*

### シェーダー
ゲーム内のすべてのオブジェクトのテクスチャは、時間の経過とともに半径が変化するガウシアンブラーシェーダに添付されています。背景もシェーダを使ってレンダリングされ、それぞれが異なるアプローチで視覚的なパターンを作成します。

![positron-world2](https://img.itch.zone/aW1hZ2UvNDQyMzE3LzI0ODg2NzYuZ2lm/original/QyUIT7.gif)

*図4.8：ワールド2の例（速度x2.0）*

![positron-world3](https://img.itch.zone/aW1hZ2UvNDQyMzE3LzI0ODg3MjguZ2lm/original/kA0%2Fib.gif)

*図4.9：ワールド3の例（速度x2.0）*


## ビデオデモ
このYouTubeのビデオは、ワールド1のためのウォークスルーを実演しています。
https://www.youtube.com/watch?v=JqT7z5t3JaM

[![positron-demo](https://img.youtube.com/vi/JqT7z5t3JaM/0.jpg)](https://www.youtube.com/watch?v=JqT7z5t3JaM)

> Written with [StackEdit](https://stackedit.io/).
