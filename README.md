# Positron
Current stable version: **1.1.0**


Positron is an on going project based on my Ludum Dare 35 entry (ShapeMagnet).

Navigate through a chaotic space with attractive and repulsive forces to find escape to neutrality.

**Links**
- README 日本語版: [README_jp.md](README_jp.md)
- Itch.io store: https://nihilaleph.itch.io/positron
- Gitlab repository: https://gitlab.com/nihilaleph/positron
- 企画書(JP only)：https://docs.google.com/presentation/d/1TnRb9mmyRT5vyNshshot34NmcEd-uYazP40jCUkIkRk/edit?usp=sharing

**Table of Content**

[[_TOC_]]

## Overview
This game is a physics based action puzzle, in which the player has the ability to alternate between a Positive and a Negative shape. Depending on which shape the player is, other agents in the level are either attracted or repelled by the player. It is inspired by other physics puzzle games, such as Portal and Legend of Zelda, and stylistic by electronic, neon visual, such as Geometry Wars.
## Mechanics

### Agents
The agents of this game are not actively aggressive towards the player. All agents, including the player, possess a **charge**, being either **positive** or **negative**. They move with impulse and rotate accordingly to the impulse. They can also die, which triggers various effects.

![positron-class](https://drive.google.com/uc?id=1J394WnGvU65bp760eITv9rC4UzvzndVk)

*Figure 2.1: UML Class Diagram of all agents*

#### Player
The player control a single body that has the power to change it's charge. They can move using the analog stick or arrow keys of a keyboard, and the designated action button (A, X, Space bar...) performs the change.

![positro-player](https://lh4.googleusercontent.com/Cc6igp5rQgSMwgBTrocNJfOomBrFBWnHrdOyv3QsIR4ml3FT733ud5Jy2cXAA4_Rxnckcc0t5Z0f6QwLgP9gOAiqXMRBZZtDm3W3bzsisUHMalTjs5_LBWKWZ4ic9Tz2L89z2dS-)

*Figure 2.2: Negative and Positive shapes of the Player*

#### Enemies
Enemies' movements only depend on their *position* and their *charge* compared to the player, much like electrical forces.  They are either repelled or attracted  by the player if the charges are the same or different, respectively. This interaction is stronger the closer the agent is to the player. They have different interactions when hitting with each other and bounce when touching the wall.

 - Simple Enemy: Dies with one hit with different charge or the player

![positro-enemy](https://lh5.googleusercontent.com/dmIkjtT0OW6J3epwGpHovBwN2kqKw0uoZjwp14eTXFztdfNbNqC19Yd5ozU8mUh9E6ltjxmkMMpzYUJcQi7ZGfCA2FrbtfGdlWKxy3foS4n_pPCrih9ATfDXdTHwOwVseDmhTFg4)

*Figure 2.3: Negative and Positive Simple Enemy*

 - Strong Enemy: Dies with 3 hits with different charge or player 

![positron-strong](https://drive.google.com/uc?id=18dvV0-9NHnz1RP0my8xeu0XDO3YHDZ6g)

*Figure 2.4: Negative and Positive Strong Enemy*

 - Block Enemy: Doesn't die and has many interaction points
 
![positron-block](https://drive.google.com/uc?id=1CvFqmdDa5qFGxyyNOK4Gy-mYeD3sQHce)

*Figure 2.5: Negative and Positive Block Enemy*

 - Unstoppable Enemy: Doesn't die nor decelerate

![positron-unstoppable](https://drive.google.com/uc?id=1J5uQS-LJDJGat2i1u0PDhy25dtCoRPek)

*Figure 2.6: Negative and Positive Unstoppable Enemy*
 
 - Blackhole Enemy: Aura that interacts with all agents and destroys them on center
 
![positron-blackhole](https://drive.google.com/uc?id=1nqaXC-IVSNPNXBgv3DLDAW1390HM_U1g)

*Figure 2.7: Negative and Positive Blackhole Enemy*

 
## Level Design
The objective of the player is to reach a portal that leads them to the next level without touching either the walls or agents. If the player touches them, they die, and have the option to restart the level. Many levels require the player to retrieve a series of keys in order to fully open the portal.

![positron-class](https://drive.google.com/uc?id=1A-49l-rhH-o7cBUIX4W0JUTujDM_7zAQ)

*Figure 3.1: Screenshot with annotations of objects and interactions*

The game is composed by 4 **worlds**, and each world contains 10 **levels**. The earlier of levels in a world is used to present to the player new ideas, while the latter levels deepens the challenge. There is no tutorial nor instructions, so the player is free to explore and find their own way to complete the level.

Every time the player completes a world, their record will be saved and they have to option to proceed to the next world.

All levels are composed with at most 3 keys, a portal, the player, a parallax background, a tile map defining the walls, the game controller and control menus.

![positron-screenshot](https://img.itch.zone/aW1hZ2UvNDQyMzE3LzI0OTI3NzYuZ2lm/original/KAdpoD.gif)

*Figure 3.2: World 4 Level 8 example (speed x2.0)*

 - World 0: Simple Enemies and Keys
 - World 1: Block Enemies and Strong Enemies
 - World 2: Blackhole Enemies and Unstoppable Enemies
 - World 3: Final Challenges

## Interface
### HUD
The title menu of the game leads to a World selection menu with the best record time, instructions of how to control, and the option to exit the game. In-game menus include a pause menu, a post-death menu, and record input menu when the player finishes a world.

![positron-title-menu](https://drive.google.com/uc?id=1bMsMDe5chgAJREkRXaEiVlSIGf9GHnhs)

*Figure 4.1: Title menu*

![positron-control](https://drive.google.com/uc?id=1S6Aw2utB5X5qppx-4MrgGSDm564lTHro)

*Figure 4.2: How to control menu*

![positron-world-menu](https://drive.google.com/uc?id=1kmKxDCDrbCxYpuiJoMOHrlxHptuIMxKi)

*Figure 4.3: World selection menu*

There is no visible HUD during the gameplay to not distract the player from what's happening in the game.

A player may freely pause the game, prompting with the options of resume playing, retry the level, or return to the title menu.

![positron-pause-menu](https://drive.google.com/uc?id=16yzX3pVgiIklOiqlvYcuYCniYJMUF61K)

*Figure 4.4: Pause menu*

If the player dies, a menu will prompt with the options of retry the level or return to the title menu. 

![positron-post-death-menu](https://drive.google.com/uc?id=1MVyzKU79tZUP-_vkXsagnxHeJASPfliq)

*Figure 4.5: Post-death menu*

If the player finishes a world, their record time of completion is shown, and they may input 3 characters to save the record. A menu is then presented with the best record and the current record, as well as the option to go to the next world or return to the title menu.

![positron-record](https://drive.google.com/uc?id=1sij8LVyw_CjKEWM-7BWq0YpQln2NNLCK)

*Figure 4.6: Record input menu*

![positron-record-menu](https://drive.google.com/uc?id=1QMJY9JO7L3it0lH0kX2jrVrsp_noJ2qc)

*Figure 4.7: Next World menu*

### Shaders
All objects in the game have their textures attached to a Gaussian-blur shader that varies it's radius with time. The background is also rendered using shaders, each using a different approach to create the visual patterns.

![positron-world2](https://img.itch.zone/aW1hZ2UvNDQyMzE3LzI0ODg2NzYuZ2lm/original/QyUIT7.gif)

*Figure 4.8: World 2 example (speed x2.0)*

![positron-world3](https://img.itch.zone/aW1hZ2UvNDQyMzE3LzI0ODg3MjguZ2lm/original/kA0%2Fib.gif)

*Figure 4.9: World 3 example (speed x2.0)*


## Video Demo
This YouTube video demonstrate a walk-through for the world 1.
https://www.youtube.com/watch?v=JqT7z5t3JaM

[![positron-demo](https://img.youtube.com/vi/JqT7z5t3JaM/0.jpg)](https://www.youtube.com/watch?v=JqT7z5t3JaM)

> Written with [StackEdit](https://stackedit.io/).
