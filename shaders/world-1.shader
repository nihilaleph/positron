// http://glslsandbox.com/e#56655.0
shader_type canvas_item;
uniform float level = 1.0;

float remap01(float a, float b, float t) {
	return (b - t) / (b - a);
}

float remap(float a, float b, float i, float j, float t) {
	return remap01(a, b, t) * (j - i) + i;
}

void fragment() {
	// Math equation for the general shape
	vec2 pos = (UV.xy);
	pos *= 31.4;
	float val = cos(pos.x) - cos(pos.y);
	// width of shape dependent on time and level
	float col = .0001 * (level * 20.0) * (cos(TIME) + 1.2)  /abs(val);
	//COLOR = vec4(vec3(col), 1.0);

	// Intensity of color of the shape
	vec3 base = vec3(min(1.0, col));
	COLOR = vec4(base, 1.0);
	
	// Paint it in a vermillion color
	COLOR *= vec4(1.,0.1,0.1,0.6);

	// Highlight cycles through the texture 
	// (since there is repetition, UV 0.0 and 1.0 should have the same value)
	float PI = 3.14159265359;
	COLOR *= (cos((UV.x * 4.0 * PI)  + TIME / 2.0 ) + sin((UV.y * 4.0 * PI) + TIME / 5.0));

	// Small circles texture applied to the shape
	COLOR *= cos(UV.x *  500.0 *PI) * cos(UV.y *  500.0 *PI);
}
