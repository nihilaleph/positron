// http://nuclear.mutantstargoat.com/articles/sdr_fract/
// https://github.com/tinmanjuggernaut/godot-fractal-art
shader_type canvas_item;

//uniform vec2 c = vec2(0.285, 0.0);
uniform int level = 1;

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.rrr + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.rrr, clamp(p - K.rrr, 0.0, 1.0), c.y);
}

float hex( vec2 p )
{		
  p.y += mod( floor(p.x), 4.0) * 0.5;
  p = abs( fract(p)- 0.5 );
  return 1.-abs( max(p.x*1.5 + p.y, p.y * 2.0) - 1.0 )*4. ;
}

void fragment() {
	float PI = 3.14159265359;
  	vec2 uv = vec2(sin(UV.x * 2.0 * PI), sin(UV.y * 2.0 * PI));
	float resolution = TEXTURE_PIXEL_SIZE.x / TEXTURE_PIXEL_SIZE.y;
  	uv.x *= resolution;
 	//if (mouse.x > 0.2)
	uv /= dot(uv,uv / float(level))*.5;
  //else
   // uv *= 1.0;
	uv.x+=TIME*0.4;
	uv.y+=TIME*0.6;
	float c = hex(uv);
	vec3 hsv = hsv2rgb(vec3(( cos(UV.x * 2.0 * PI + TIME / 10.0 * float(level))+sin(UV.y * 2.0 * PI + TIME / 30.0 * float(level)))/2.0,0.3,0.9));
	COLOR = vec4(hsv * c, 0.6);
	COLOR *= 

	// Small circles texture applied to the shape
	COLOR *= cos(UV.x *  500.0 *PI) * cos(UV.y *  500.0 *PI);
}
