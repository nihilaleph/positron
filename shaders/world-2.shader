// http://glslsandbox.com/e#56778.0
shader_type canvas_item;
uniform float level = 1.0;

void fragment() {

	float PI = 3.14159265359;
	
	// Repetition in one texture size
	vec2 position = UV * 4.0;
	
	// Math equation for the general shape
	float color = sin( sin(PI * position.x) * cos(TIME / 3.0 ) * 8.0/ 4.0 * level) + cos( sin(PI * position.y) * sin( TIME / 15.0 ) * 1.0/ 4.0  * level);
	color *= sin( sin(PI * position.y) + sin( TIME / 25.0 ) * 4.0/ 4.0 * level ) + cos(  sin(PI * position.x) + cos( TIME  / 4.0 ) * 4.0/ 4.0  * level);
	color *= sin(  sin(PI * position.x) + sin( TIME /15.0 ) * 1.0 / 4.0* level ) + sin( sin(PI * position.y) + sin( TIME / 35.0) * 8.0/ 4.0  * level);
	//color *= sin( TIME / 10.0 ) * 0.5;

	// Intensity of color of the shape
	COLOR = vec4( vec3( min(color,1.0) ), 1.0 );
	// Paint it in a emerald color
	COLOR *= vec4(0.1,1.,0.1,0.4);
	// Small circles texture applied to the shape
	COLOR *= cos(UV.x * 500.0 *PI) * cos(UV.y * 500.0 *PI);
}


