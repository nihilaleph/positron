// http://nuclear.mutantstargoat.com/articles/sdr_fract/
// https://github.com/tinmanjuggernaut/godot-fractal-art
shader_type canvas_item;

//uniform vec2 c = vec2(0.285, 0.0);
uniform int level = 1;
uniform vec2 seed = vec2(-0.794084, 0.136444);
uniform float scale = .25;
uniform vec2 position = vec2(1.0);

void fragment() {
	int iter = 1 + level;
	float PI = 3.14159265359;
	float resolution = TEXTURE_PIXEL_SIZE.x / TEXTURE_PIXEL_SIZE.y;
	
	// Julia fractal calculation with oscilating points
    vec2 z = UV;
	z.x = (cos(UV.x * 2.0 * PI + TIME * 0.0143 * (scale + float(level) / 5.0 ))) ;
	z.y = (sin(UV.y * 2.0 * PI + TIME * 0.3286 * (scale + float(level) / 5.0 ))) ;
	
	//z = UV / scale - position;
    
	vec2 c;
	c.x = sin(TIME + sin(UV.y * 2.0 * PI) * 3.27) + cos(TIME * 1.24 + cos(UV.y * 2.0 * PI) * 3.27) * seed.x;
	c.y = cos(TIME + cos(UV.x * 2.0 * PI) * 14.773) + sin(TIME * 1.645 + cos(UV.x * 2.0 * PI) * 2.234) * seed.y;
	
	//c = seed;

    int i;
    for(i=0; i<iter; i++) {
        float x = (z.x * z.x - z.y * z.y) + c.x;
        float y = (z.y * z.x + z.x * z.y) + c.y;

        if((x * x + y * y) > 4.0) break;
        z.x = x;
        z.y = y;
    }

    COLOR = vec4(vec3(i == iter ? 0.0 : float(i)) / 4.0, 1.0);
	
	// Paint it in a teal color
	COLOR *= vec4(0.0,1.,1.,.6);
	// Small circles texture applied to the shape
	COLOR *= cos(UV.x * 500.0 *PI) * cos(UV.y * 500.0 *PI);
	
}
