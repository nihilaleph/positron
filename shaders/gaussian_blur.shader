shader_type canvas_item;

uniform float PixelSize : hint_range(0.0, 1.0) = 0.001;

uniform int Steps : hint_range(1, 10) = 8;
uniform float Sigma : hint_range(0.0, 5.0) = 1;
uniform float BlurRange : hint_range(0, 1024) = 8;
uniform int ArcsNumber : hint_range(1, 64) = 12;

float blurWeight(float x) {
    return exp(-0.5 * (x * x) / (Sigma * Sigma));
}

vec4 blur(sampler2D text, vec2 coord, float blurRange, float resolution, float pixelSize) {
	float PI  = 3.14159265358979323846264; // PI
	vec4 total = vec4(0.0);
	float totalWeight = 0.0;
	for (int i = 1; i <= Steps; ++i) {
		for (int arc = 0; arc < ArcsNumber; arc++) {
			float teta = 360.0 / float(ArcsNumber) * float(arc);
			vec2 texCoord = coord + vec2(cos(teta)  * resolution, sin(teta)) * float(i) / float(Steps) * blurRange * pixelSize;
			float weight = blurWeight(float(i)/float(Steps));
			total += weight * texture(text, texCoord);
			totalWeight += weight;
		}
    }
    return total / totalWeight;
}

void fragment() {
	float resolution = TEXTURE_PIXEL_SIZE.x / TEXTURE_PIXEL_SIZE.y;
	vec2 pixel_size = 1.0 / vec2(textureSize(TEXTURE, 0));
	COLOR = texture(TEXTURE, UV);
	//COLOR += blur(TEXTURE, UV, BlurRange, resolution, pixel_size.y);
	COLOR += blur(TEXTURE, UV, BlurRange, resolution, PixelSize);
	//COLOR += blur(TEXTURE, UV, BlurRange * 2.0) / 2.0;
}