shader_type canvas_item;

uniform sampler2D noise: hint_black;
uniform float propagation : hint_range(0.1, 10.0) = 0.5;
uniform float amplitude : hint_range(0.1, 500.0) = 200.0;

void fragment() {
	vec2 newUV = UV + vec2(texture(noise, vec2(sin(TIME + UV.x / 2.0) / propagation + 0.5, 0.0)).x, texture(noise, vec2(0.0, cos(TIME + UV.y / 2.0) / propagation + 0.5)).x) / amplitude;
	COLOR = texture(TEXTURE, newUV);
	//COLOR.rb = vec2(texture(noise, vec2(sin(TIME + UV.x / 2.0) / propagation + 0.5, 0.0)).x, texture(noise, vec2(0.0, cos(TIME + UV.y / 2.0) / propagation + 0.5)).x) / amplitude;
	//COLOR.ga = vec2(0.0,1.0);
}
