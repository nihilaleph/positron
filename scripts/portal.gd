extends Area2D


# Some particles to explode
export var implosion_scene = preload("res://scenes/particles/implosion.tscn")

signal player_entered_portal(portal)
signal player_transported()

# Set if the keys were picked up
var key_r_picked = false
var key_g_picked = false
var key_b_picked = false

# Set if it was activated by the Player
var is_active = false
# Holds target reference (Player)
var target

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _physics_process(delta):
	# Process if it was activated by Player
	if is_active && is_instance_valid(target):
		# Suck player while shrinking both sizes
		target.global_position = lerp(target.global_position, global_position, 0.2)
		target.global_scale *= .80
		global_scale *= .92
		# When small enough, finish level and change scene
		if target.global_scale.length_squared() < 0.01 :
			emit_signal("player_transported")
			is_active = false
			
func _on_Portal_body_entered(body):
	# Only reacts to player if all keys were picked
	if key_r_picked && key_g_picked && key_b_picked && body.name == "Player":
		target = body.get_node("CollisionShape2D")
		is_active = true
		
		# Some particles
		var implosion = implosion_scene.instance()
		implosion.global_position = global_position
		implosion.emitting = true
		get_tree().get_root().add_child(implosion)
		
		# Play transportation soundeffect
		$AudioStreamPlayer2D.play()
		
		emit_signal("player_entered_portal", self)
		
	pass # Replace with function body.
	
func _on_KeyR_picked_up():
	$Sprite.modulate.r = 1.0
	$Particles2DR.emitting = true;
	key_r_picked = true
	
func _on_KeyG_picked_up():
	$Sprite.modulate.g = 1.0
	$Particles2DG.emitting = true;
	key_g_picked = true
	
func _on_KeyB_picked_up():
	$Sprite.modulate.b = 1.0
	$Particles2DB.emitting = true;
	key_b_picked = true
