extends VBoxContainer


# Index of menu selection
var index = 0
var options
var animations

export(String, FILE, "*.tscn") var next_scene

# Flag if menu is active
export (bool) var active = false

# Autoload variables
var stopwatch

# Called when the node enters the scene tree for the first time.
func _ready():
	# Get menu options
	options = get_children()
	
	stopwatch = get_node("/root/Stopwatch")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Prcoess input only if menu is active
	if !active:
		return
		
	# Navigation input
	if Input.is_action_just_pressed("ui_down"):
		options[index].modulate.a = .5
		index += 1
		get_node("/root/TitleScreen/AudioStreamPlayer").play()
		if index >= options.size():
			index = 0
		
		options[index].modulate.a = 1.0
		options[index].grab_focus()
		
	if Input.is_action_just_pressed("ui_up"):
		options[index].modulate.a = .5
		index -= 1
		get_node("/root/TitleScreen/AudioStreamPlayer").play()
		if index < 0:
			index = options.size() - 1
		
		options[index].modulate.a = 1.0
		options[index].grab_focus()
	
	# Selection input
	#if Input.is_action_just_pressed("ui_action"):
	#	match index:
	#		0:
	#			# Opens world selection menu
	#			start()
	#		1:
	#			# Opens Control menu
	#			control()
	#		2:
	#			# Close game
	#			quit()
				
func start():
	# Opens world selection menu
	get_node("/root/TitleScreen/AnimationPlayer").play("title-world")
	
func control():
	# Opens Control menu
	get_node("/root/TitleScreen/AnimationPlayer").play("title-control")
	
func quit():
	# Close game
	get_tree().quit()


func _on_Play_pressed():
	start()
	pass

func _on_Control_pressed():
	control()
	pass

func _on_Quit_pressed():
	quit()
	pass


func _on_Play_mouse_entered():
	highlight_option(0)

func _on_Control_mouse_entered():
	highlight_option(1)
	
func _on_Quit_mouse_entered():
	highlight_option(2)
	
	
func highlight_option(i):
	for child in options:
		child.modulate.a = 0.5
	index = i
	options[index].modulate.a = 1.0
	options[index].grab_focus()
	
func reactivate_menu():
	if active:
		active = false
		options[index].release_focus()
	else:
		active = true
		options[index].grab_focus()



