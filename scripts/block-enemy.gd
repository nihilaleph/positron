extends "simple-enemy.gd"

# Hold particle materials for each component
var particle_materials = []

# Override
func _ready():
	initial_collision_shape_rotation = $CollisionShape2D.rotation_degrees
	
	# Make material unique for each component in this instance (for trail)
	for collision_shape in get_children():
		if collision_shape.get_class() == "CollisionShape2D":
			var particle_material = collision_shape.get_node("Sprite/Particles2D").process_material.duplicate(true)
			particle_materials.push_back(particle_material)
			collision_shape.get_node("Sprite/Particles2D").process_material = particle_material
	pass # Replace with function body.
	
# Override	
func _process(delta):
	# Update every particle materials (for trail)
	for particle_material in particle_materials:
		particle_material.angle = initial_collision_shape_rotation - rotation_degrees
		pass
		
# Override
func _integrate_forces(state):
	# Get contact point if collided
	if state.get_contact_count() > 0:
		collision_position = state.get_contact_collider_position(0)
		collision_normal = state.get_contact_local_normal(0)
		
	# Find player to apply impulse
	var players = get_tree().get_nodes_in_group("players")
	if players.size() > 0 && !players[0].killed:
		var player = players[0]
		
		var s = -1 if player.charge == charge else 1
		
		# Get all interaction points and apply charge impulse on them
		for collision_shape in get_children():
			if collision_shape.get_class() == "CollisionShape2D":
				for point in collision_shape.get_node("InteractionPoints").get_children():
					var distance = player.global_position - point.global_position
					
					var force = s * distance.normalized() * CHARGE_IMPULSE / (distance.length_squared())
					apply_impulse(point.global_position - global_position, force)
				
# When hit something friendly, bounce, otherwise consider a hit
func _on_Agent_body_entered(body):
	._on_Agent_body_entered(body)
	match body.get_name():
		"Player":
			hit()
		"TileMap":
			bounce_to(BOUNCE_IMPULSE, collision_normal.normalized(), to_local(collision_position))
		_:
			if body.charge == charge:
				bounce_to(BOUNCE_IMPULSE, collision_normal.normalized(), to_local(collision_position))
			else:
				hit()	
				
func hit():
	# When hitting some aggressive body, apply impulse
	bounce_to(DAMAGE_IMPULSE, collision_normal.normalized(), to_local(collision_position))
	