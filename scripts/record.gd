extends Node


# Keep records
var saved_records

# Called when the node enters the scene tree for the first time.
func _ready():
	# Default structure
	saved_records = {
		"version" : 1,
		"1": [],
		"2": [],
		"3": [],
		"4": []
	}
	pass # Replace with function body.
	
func add_record(world, elapsed_time, name):
	var new_record = {
		"name": name,
		"elapsed_time": elapsed_time,
		"online" : false,
	}
	saved_records[world].push_back(new_record)
	saved_records[world].sort_custom(RecordSorter, "sort")
	
	return new_record

class RecordSorter:
    static func sort(a, b):
        if a["elapsed_time"] < b["elapsed_time"]:
            return true
        return false

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

## From stopwatch.gd
# Gets the current elapsed time as a formatted string. The default format is HH:mm:ss:SSS.
# Parameters:
#	elapsed_time: value to be formatted
#	f_hours: The format to use to represent hours. This can be a String or null.
#	f_minutes: The format to use to represent minutes. This can be a String or null.
#	f_seconds: The format to use to represent seconds. This can be a String or null.
#	f_millis: The format to use to represent milliseconds. This can be a String or null.
#	millis_precision: The number of digits to return for milliseconds.
func get_formatted_elapsed_time(elapsed_time, f_minutes = "%02d:", f_seconds = "%02d:", f_millis = "%03d", millis_precision = 3):
	# Get the current elapsed time.
	var time = elapsed_time
	
	# Break the elapsed time into seconds and milliseconds.
	var part_seconds = 0
	var part_milliseconds = 0
	var time_split = str(time).split(".")
	if time_split.size() == 2:
		part_seconds = int(time_split[0])
		part_milliseconds = int(time_split[1].substr(0, millis_precision))
	
	# Break the total seconds into their respective time parts.
	#var hours = floor(part_seconds / 3600)
	#var minutes = fmod(floor(part_seconds / 60), 60)
	var minutes = floor(part_seconds / 60)
	var seconds = fmod(part_seconds, 60)
	var millis = part_milliseconds
	
	# Prepare output formatting.
	var format_string = ""
	var format_values = Array()
	#if f_hours != null and f_hours != "":
	#	format_string += f_hours
	#	format_values.append(hours)
	if f_minutes != null and f_minutes != "":
		format_string += f_minutes
		format_values.append(minutes)
	if f_seconds != null and f_seconds != "":
		format_string += f_seconds
		format_values.append(seconds)
	if f_millis != null and f_millis != "":
		format_string += f_millis
		format_values.append(millis) 
	
	# Return the formatted output.
	return format_string % format_values