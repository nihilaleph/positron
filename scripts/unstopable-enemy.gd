extends "simple-enemy.gd"

# Simulation parameters
export var init_vel = Vector2(500, 0)
export var max_speed = 700

# Called when the node enters the scene tree for the first time.
func _ready():
	linear_velocity = init_vel
	# Set animation according to charge
	match charge:
		CHARGE.POSITIVE:
			$CollisionShape2D/Sprite/AnimationPlayer.play_backwards("rotation")
		CHARGE.NEGATIVE:
			$CollisionShape2D/Sprite/AnimationPlayer.play("rotation")
	
	pass # Replace with function body.
	
func _integrate_forces(state):
	._integrate_forces(state)
	# Clamp linear velocity
	if linear_velocity.length_squared() > max_speed * max_speed:
		linear_velocity = linear_velocity.normalized() * max_speed
	
# Hit only bounces
func hit():
	bounce_to(DAMAGE_IMPULSE, (global_position - collision_position).normalized())

func bounce_to(impulse, direction, offset = Vector2.ZERO):
	play_bound_effect(impulse, direction, offset)

# No rotation for unstopable enemy
func apply_impulse_with_rotation(offset, impulse):
	apply_impulse(offset, impulse)
