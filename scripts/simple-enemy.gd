extends "agent.gd"

# Parameters for simulation
export var CHARGE_IMPULSE = 1000000.0
export var BOUNCE_IMPULSE = 500.0
export var DAMAGE_IMPULSE = 1000.0
# Collision variables
var collision_position
var collision_normal
# Particles scene reference
export var repulsion_scene = preload("res://scenes/particles/repulsion.tscn")
export var repulsion_particle_negative = preload("res://scenes/particles/particle-materials/repulsion-negative.tres")
export var repulsion_particle_positive = preload("res://scenes/particles/particle-materials/repulsion-positive.tres")

# Called when the node enters the scene tree for the first time.
func _ready():
	._ready()
		
	pass # Replace with function body.

func _process(delta):
	._process(delta)
	
func _integrate_forces(state):
	# Get contact point if collided
	if state.get_contact_count() > 0:
		collision_position = state.get_contact_collider_position(0)
		collision_normal = state.get_contact_local_normal(0)
		
	# Find player to apply impulse
	var players = get_tree().get_nodes_in_group("players")
	if players.size() > 0 && !players[0].killed:
		var player = players[0]
		
		var s = -1 if player.charge == charge else 1
		
		var distance = player.global_position - global_position
		
		var force = s * distance.normalized() * CHARGE_IMPULSE / (distance.length_squared())
	
		apply_impulse_with_rotation(Vector2.ZERO, force)
	
# When hit something friendly, bounce, otherwise consider a hit
func _on_Agent_body_entered(body):
	._on_Agent_body_entered(body)
	match body.get_name():
		"Player":
			hit()
		"TileMap":
			bounce_to(BOUNCE_IMPULSE, (global_position - collision_position).normalized())
		_:
			if body.charge == charge:
				bounce_to(BOUNCE_IMPULSE, (global_position - collision_position).normalized())
			else:
				hit()
	#die()

# What happens to agent after being hit
func hit():
	die()
	
func bounce_to(impulse, direction, offset = Vector2.ZERO):
	play_bound_effect(impulse, direction, offset)
	apply_impulse(offset, direction * impulse)

# Effects when bouncing
func play_bound_effect(impulse, direction, offset = Vector2.ZERO):
	var repulsion = repulsion_scene.instance()
	repulsion.global_position = collision_position
	repulsion.emitting = true
	repulsion.global_rotation = direction.angle() + 90.0 - 45.0
	match charge:
		CHARGE.POSITIVE:
			repulsion.process_material = repulsion_particle_positive
		CHARGE.NEGATIVE:
			repulsion.process_material = repulsion_particle_negative
	
	get_tree().get_root().add_child(repulsion)
	
	repulsion = repulsion_scene.instance()
	repulsion.global_position = collision_position
	repulsion.emitting = true
	repulsion.global_rotation = direction.angle() - 90.0 + 45.0
	match charge:
		CHARGE.POSITIVE:
			repulsion.process_material = repulsion_particle_positive
		CHARGE.NEGATIVE:
			repulsion.process_material = repulsion_particle_negative
	
	get_tree().get_root().add_child(repulsion)
	
	$AudioStreamPlayer2D.play()	