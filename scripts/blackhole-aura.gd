extends Area2D

# Enum for agent charge
enum CHARGE { NEGATIVE, POSITIVE }

# Agent charge
export(CHARGE) var charge = CHARGE.POSITIVE

# Bodies inside aura
var bodies = []
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Aura_body_entered(body):
	bodies.push_back(body)
	pass # Replace with function body.


func _on_Aura_body_exited(body):
	pass # Replace with function body.
