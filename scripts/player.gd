extends "agent.gd"

# Input variables
var HorizontalAxis = 0.0
var VerticalAxis = 0.0

# Movement parameters
export var Acceleration = 30.0
export var MaxSpeed = 300
export var Invincible = false

# Checks if player is still in transforming state
var transforming = false
# Holds button for tranformation
export(NodePath) var transforming_button_path
var transforming_button

# Called when the node enters the scene tree for the first time.
func _ready():
	._ready()
	# Add player to players group for easier reference
	add_to_group("players")
	# Trigger when animation is finished for transforming flag
	$CollisionShape2D/Sprite/AnimationPlayer.connect("animation_finished", self, "_on_AnimationPlayer_animation_finished")
	
	transforming_button = get_node(transforming_button_path)
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	._process(delta)
	
	# Navigation input
	HorizontalAxis = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	VerticalAxis = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	
	# Navigation input through mouse/touchscreen
	if Input.is_action_pressed("ui_target"):
		var direction = get_global_mouse_position() - global_position
		var intensity = min(direction.length() / 400.0, 1.0)
		direction = direction.normalized() * intensity
		
		HorizontalAxis = direction.x
		VerticalAxis = direction.y
	
	# Transform input (only if not transforming already)
	if Input.is_action_just_pressed("ui_action"):
		henshin()
				

func henshin():
	if transforming:
		return
	transforming_button.disabled = true
	$HenshinAudioStreamPlayer2D.play()
	match charge:
		CHARGE.POSITIVE:
			$CollisionShape2D/Sprite/AnimationPlayer.play_backwards("negative-positive");
			charge = CHARGE.NEGATIVE
			transforming = true
		CHARGE.NEGATIVE:
			$CollisionShape2D/Sprite/AnimationPlayer.play("negative-positive");
			charge = CHARGE.POSITIVE
			transforming = true

func _integrate_forces(state):
	# Only apply impulse if velocity is not maxed
	if linear_velocity.length_squared() < MaxSpeed * MaxSpeed :
		var force = Vector2(HorizontalAxis, VerticalAxis)
		if force.length_squared() > 1:
			force = force.normalized()
	
		apply_impulse_with_rotation(Vector2.ZERO, force * Acceleration)

# Die to any physical contact
func _on_Agent_body_entered(body):
	._on_Agent_body_entered(body)
	if !Invincible:
		die()
	#get_tree().reload_current_scene()
	pass # Replace with function body.
	
func _on_AnimationPlayer_animation_finished(animation_name):
	transforming = false
	transforming_button.disabled = false

func _on_Portal_player_transported():
	queue_free()


func _on_TextureButton_pressed():
	henshin()
