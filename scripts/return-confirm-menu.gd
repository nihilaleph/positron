extends Control

# Signal to Game controller to cancel return
signal cancel_action

# Block first visible frame
var frame_block = true

# Menu options variable
var index = 1
var options

# Called when the node enters the scene tree for the first time.
func _ready():
	# Populate menu options
	options = $NinePatchRect/CenterContainer/HBoxContainer.get_children()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Only process input if menu is active
	# Paused state
	if !visible:
		frame_block = true
		return
	# Ignore first frame after being visible
	elif frame_block:
		frame_block = false
		return
		
	# Navigation input
	if Input.is_action_just_pressed("ui_right"):
		options[index].modulate.a = .5
		index += 1
		$AudioStreamPlayer.play()
		
		if index >= options.size():
			index = 0
			
		options[index].modulate.a = 1.0
		options[index].grab_focus()
		
	if Input.is_action_just_pressed("ui_left"):
		options[index].modulate.a = .5
		index -= 1
		$AudioStreamPlayer.play()
		if index < 0:
			index = options.size() - 1
			
		options[index].modulate.a = 1.0
		options[index].grab_focus()
			
	# Select option			
	if Input.is_action_just_pressed("ui_action"):
		match index:
			# Return to title screen
			0:
				get_tree().paused = false
				get_tree().change_scene("res://scenes/title-screen.tscn")
			# Open previous menu
			1:
				emit_signal("cancel_action")

func highlight_option(i):
	for child in options:
		child.modulate.a = 0.5
	index = i
	options[index].modulate.a = 1.0
	options[index].grab_focus()
	
func yes():
	get_tree().paused = false
	get_tree().change_scene("res://scenes/title-screen.tscn")
	
func no():
	emit_signal("cancel_action")
	

func _on_ReturnConfirmMenu_visibility_changed():
	highlight_option(1)


func _on_Yes_pressed():
	yes()

func _on_No_pressed():
	no()


func _on_Yes_mouse_entered():
	highlight_option(0)

func _on_No_mouse_entered():
	highlight_option(1)
