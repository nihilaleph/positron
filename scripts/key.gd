extends Area2D

signal picked_up

# Set if this keys starts already picked
export(bool) var picked = false
export var pickup_audio = preload("res://scenes/audiostreams/pickup.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	# Dispose of object if already picked
	if picked:
		emit_signal("picked_up")
		queue_free()
	pass # Replace with function body.

func _on_Key_body_entered(body):
	# Only reacts to player
	if body.name == "Player":
		picked = true
		emit_signal("picked_up")
		queue_free()
		
		# Play pickup sound
		var pickup = pickup_audio.instance()
		pickup.global_position = global_position
		pickup.play()
		get_tree().get_root().add_child(pickup)
		
	pass # Replace with function body.