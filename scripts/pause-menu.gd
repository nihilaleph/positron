extends Control

# Signal to Game controller desire to return
signal return_title

# Menu options variable
var index = 0
var options
# Called when the node enters the scene tree for the first time.
func _ready():
	# Populate menu options
	options = $NinePatchRect/CenterContainer/VBoxContainer.get_children()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Paused state
	if !visible:
		return
		
	# Navigation input
	if Input.is_action_just_pressed("ui_down"):
		options[index].modulate.a = .5
		index += 1
		$AudioStreamPlayer.play()
		
		if index >= options.size():
			index = 0
			
		options[index].modulate.a = 1.0
		options[index].grab_focus()
		
	if Input.is_action_just_pressed("ui_up"):
		options[index].modulate.a = .5
		index -= 1
		$AudioStreamPlayer.play()
		if index < 0:
			index = options.size() - 1
			
		options[index].modulate.a = 1.0
		options[index].grab_focus()
			
	# Select option			
	if Input.is_action_just_pressed("ui_action"):
		match index:
			0:
			# Close menu
				get_tree().paused = false
				hide()
			1:
			# Reload level
				get_tree().paused = false
				get_tree().reload_current_scene()
			2:
			# Return to title
				emit_signal("return_title", self)
				

func highlight_option(i):
	for child in options:
		child.modulate.a = 0.5
	index = i
	options[index].modulate.a = 1.0
	options[index].grab_focus()
	
func resume():
	get_tree().paused = false
	hide()
	
func retry():
	get_tree().paused = false
	get_tree().reload_current_scene()
	
func back():
	emit_signal("return_title", self)

func _on_PauseMenu_visibility_changed():
	highlight_option(0)


func _on_Resume_pressed():
	resume()
	options[index].release_focus()


func _on_Retry_pressed():
	retry()
	options[index].release_focus()


func _on_ReturnToTitle_pressed():
	back()
	options[index].release_focus()


func _on_Resume_mouse_entered():
	highlight_option(0)


func _on_Retry_mouse_entered():
	highlight_option(1)


func _on_ReturnToTitle_mouse_entered():
	highlight_option(2)
