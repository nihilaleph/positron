extends Node2D

# Reference to next level
# 	If empty, mean it's last level of the world
export(String, FILE, "*.tscn") var next_scene

# What shows what is the world of the level
export var world = 1

# Timer for displaying death menu
var player_died_timer
var player_died = false

# Timer for changing scene
var change_scene_timer

# Hold menu before a confirmation window
var previous_menu

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	# Check for pause input
	if Input.is_action_just_pressed("ui_menu") && !player_died:
		game_pause()
	elif Input.is_action_just_pressed("ui_cancel") && get_tree().paused && !player_died:
		if $CanvasLayer/PauseMenu.is_visible():
			$CanvasLayer/PauseMenu.hide()
			get_tree().paused = false
		else:
			$CanvasLayer/ReturnConfirmMenu.hide()
			previous_menu.show()
	pass
	
func game_pause():
	if get_tree().paused:
		if $CanvasLayer/PauseMenu.is_visible():
			$CanvasLayer/PauseMenu.hide()
			get_tree().paused = false
		else:
			$CanvasLayer/ReturnConfirmMenu.hide()
			previous_menu.show()
	else:
		$CanvasLayer/PauseMenu.show()
		get_tree().paused = true
	
# Trigger for player death
func _on_Player_died():
	# Set timer to change scene
	player_died_timer = Timer.new()
	player_died_timer.one_shot = true
	player_died_timer.connect("timeout",self,"_on_Player_died_timer") 
	add_child(player_died_timer)
	
	player_died_timer.start(1.0)
	
	player_died = true
	pass # Replace with function body.
	
func _on_Player_died_timer():
	$CanvasLayer/DeathMenu.show()
	
func _on_Portal_player_transported():
	# If there is next scene, load it
	if next_scene:
		# Set timer to change scene
		change_scene_timer = Timer.new()
		change_scene_timer.one_shot = true
		change_scene_timer.connect("timeout",self,"_on_change_scene_timer_timeout") 
		add_child(change_scene_timer)
		change_scene_timer.start(2.0)
	# If not, the world is finished, display record
	else:
		var stopwatch = get_node("/root/Stopwatch")
		stopwatch.stop()
		$CanvasLayer/RecordInput.show()
		
# Trigger to change to next level
func _on_change_scene_timer_timeout():
	print(get_node("/root/Stopwatch").get_formatted_elapsed_time())
	get_tree().change_scene(next_scene)

# Trigger after name was input for record
func _on_RecordInput_name_entered(name):
	$CanvasLayer/RecordInput.hide()
	var stopwatch = get_node("/root/Stopwatch")
	var records = get_node("/root/Records")
	
	# Save new record into file
	var new_record = records.add_record(str(world), stopwatch.get_elapsed_time(), name)
	save_game()
	
	$CanvasLayer/RecordMenu.update_record(world, new_record)
	$CanvasLayer/RecordMenu.show()
	
# Open confirmation menu when prompt to return to title
func _on_ReturnToTitle(menu):
	previous_menu = menu
	menu.hide()
	$CanvasLayer/ReturnConfirmMenu.show()
	
# Reopens previous menu if cancelled
func _on_CancelAction():
	$CanvasLayer/ReturnConfirmMenu.hide()
	previous_menu.show()
	
	
# Save game file with records
func save_game():
	var save_game = File.new()
	save_game.open("user://savegame.save", File.WRITE)
	save_game.store_string(JSON.print(get_node("/root/Records").saved_records, "  ", true))
	save_game.close()
	


func _on_Button_pressed():
	game_pause()
	$CanvasLayer/UI/PauseButton.release_focus()
	


func _on_TextureButton_pressed():
	pass # Replace with function body.
