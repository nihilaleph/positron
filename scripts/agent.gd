extends RigidBody2D

# Enum for agent charge
enum CHARGE { NEGATIVE, POSITIVE }

# Temporaries scenes to be instanced
export var explosion_scene = preload("res://scenes/particles/explosion.tscn")
export var explosion_particle_negative = preload("res://scenes/particles/particle-materials/explosion-negative.tres")
export var explosion_particle_positive = preload("res://scenes/particles/particle-materials/explosion-positive.tres")
export var death_audio = preload("res://scenes/audiostreams/death.tscn")

# Signal emitted when agent dies
signal died

# Agent charge
export(CHARGE) var charge = CHARGE.POSITIVE
# Agent was killed flag
var killed = false

# Collision Shape initial rotation (for trail particles)
var initial_collision_shape_rotation
# Particl material for trail
var particle_material

# Called when the node enters the scene tree for the first time.
func _ready():
	initial_collision_shape_rotation = $CollisionShape2D.rotation_degrees
	# Make material unique for this instance
	particle_material = $CollisionShape2D/Sprite/Particles2D.process_material.duplicate(true)
	$CollisionShape2D/Sprite/Particles2D.process_material = particle_material
	pass
	
func _process(delta):
	# Update trail rotation
	particle_material.angle = initial_collision_shape_rotation - $CollisionShape2D.rotation_degrees - rotation_degrees
	pass
	
# Makes sprite point towards impulse
func apply_impulse_with_rotation(offset, impulse):
	apply_impulse(offset, impulse)
	if impulse.length_squared() > .1:
		look_at_lerp(position + impulse, 0.08)
	
# Perform a look_at with larp
func look_at_lerp(target, alpha):
	# Get targeted rotation (with PI/2.0 adjustment)
	var final_rotation = position.angle_to_point(target) - PI / 2.0
	
	# Reseting rotation to between -360~360
	final_rotation = final_rotation - int(final_rotation / (2.0 * PI)) * 2.0 * PI
	$CollisionShape2D.rotation = $CollisionShape2D.rotation - int($CollisionShape2D.rotation / (2.0 * PI)) * 2.0 * PI
	
	# Invert direction of rotation if |current route| > PI
	if $CollisionShape2D.rotation - final_rotation > PI:
		final_rotation += 2 * PI
	if $CollisionShape2D.rotation - final_rotation < -PI:
		final_rotation -= 2 * PI
	
	$CollisionShape2D.rotation = lerp($CollisionShape2D.rotation, final_rotation, alpha)
	
	
# Func to kill agent
func die():
	emit_signal("died")
	queue_free()
	
	# Emit death explosion particles
	var explosion = explosion_scene.instance()
	explosion.global_position = global_position
	explosion.emitting = true
	match charge:
		CHARGE.POSITIVE:
			explosion.process_material = explosion_particle_positive
		CHARGE.NEGATIVE:
			explosion.process_material = explosion_particle_negative	
	get_tree().get_root().add_child(explosion)
	
	# Play death sound
	var death = death_audio.instance()
	death.global_position = global_position
	death.play()
	get_tree().get_root().add_child(death)
	
	
func _on_Agent_body_entered(body):
	#print(body.get_name())
	pass # Replace with function body.
