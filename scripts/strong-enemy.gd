extends "simple-enemy.gd"

# Textures according to agent life
export(Texture) var TEXTURE_LIFE_1
export(Texture) var TEXTURE_LIFE_2
# Tracks agent life
var life = 3
# Initial parameters for simulation
var initial_charge_impulse
var initial_bounce_impulse
var initial_damage_impulse
# Charge factor changed with life
var CHARGE_FACTOR = 2.0

# Called when the node enters the scene tree for the first time.
func _ready():
	initial_charge_impulse = CHARGE_IMPULSE
	initial_bounce_impulse = BOUNCE_IMPULSE
	initial_damage_impulse = DAMAGE_IMPULSE
	# Adds charge factor
	CHARGE_IMPULSE *= 1.0 + 1.0 / CHARGE_FACTOR
	BOUNCE_IMPULSE *= 1.0 + 1.0 / CHARGE_FACTOR
	DAMAGE_IMPULSE *= 1.0 + 1.0 / CHARGE_FACTOR
	pass # Replace with function body.

func hit():
	# When hit lose life
	life -= 1
	if life <= 0:
		die()
	else:
		# Bounce with damage impulse
		bounce_to(DAMAGE_IMPULSE, (global_position - collision_position).normalized())
		
		# Emit death explosion particles
		var explosion = explosion_scene.instance()
		explosion.global_position = global_position
		explosion.emitting = true
		match charge:
			CHARGE.POSITIVE:
				explosion.process_material = explosion_particle_positive
			CHARGE.NEGATIVE:
				explosion.process_material = explosion_particle_negative	
		get_tree().get_root().add_child(explosion)
		
		# Lower charge
		CHARGE_IMPULSE = CHARGE_IMPULSE - initial_charge_impulse / (2.0 * CHARGE_FACTOR)
		BOUNCE_IMPULSE = BOUNCE_IMPULSE - initial_bounce_impulse / (2.0 * CHARGE_FACTOR)
		DAMAGE_IMPULSE = DAMAGE_IMPULSE - initial_damage_impulse / (2.0 * CHARGE_FACTOR)
		
		# Change sprite
		match life:
			1:
				$CollisionShape2D/Sprite.texture = TEXTURE_LIFE_1
			2:
				$CollisionShape2D/Sprite.texture = TEXTURE_LIFE_2
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
