extends Control


signal name_entered(name)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_LineEdit_text_changed(new_text):
	# Upper case the entered text and filter to \w+
	new_text = returnSpecificSymbolsOnly(new_text.to_upper(), '\\w+')
	$NinePatchRect/CenterContainer/HBoxContainer/LineEdit.text = new_text
	$NinePatchRect/CenterContainer/HBoxContainer/LineEdit.caret_position = new_text.length()
	pass # Replace with function body.

func returnSpecificSymbolsOnly(enteredText, regex):
	var word = ''
	for letter in enteredText:
		var resultingText = RegEx.new()
		resultingText.compile(regex) #search pattern
		if resultingText.search(letter,0,-1) : word = word+letter
	return word

func _on_LineEdit_text_entered(new_text):
	emit_signal("name_entered", new_text)
	pass # Replace with function body.

func show():
	.show()
	$NinePatchRect/CenterContainer/HBoxContainer/Label.text = get_node("/root/Stopwatch").get_formatted_elapsed_time()
	$NinePatchRect/CenterContainer/HBoxContainer/LineEdit.grab_focus()
