extends VBoxContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Index of menu selection
var index = 0
var world = 1
var max_world = 4
var options
var animations

# File references to all worlds
export(String, FILE, "*.tscn") var scene1
export(String, FILE, "*.tscn") var scene2
export(String, FILE, "*.tscn") var scene3
export(String, FILE, "*.tscn") var scene4
export(Resource) var track1
export(Resource) var track2
export(Resource) var track3
export(Resource) var track4

export (bool) var active = false

# Autoload variables
var bgm
var stopwatch
var animation
var records
var current_record_value

# Called when the node enters the scene tree for the first time.
func _ready():
	#animations = $AnimationPlayer.get_animation_list()
	#$AnimationPlayer.play(animations[0])
	
	# Get menu options
	options = $WorldMenu.get_children()
	
	# Initilize Autoload variables
	bgm = get_node("/root/BGM")
	stopwatch = get_node("/root/Stopwatch")
	records = get_node("/root/Records")
	
	animation = get_node("/root/TitleScreen/AnimationPlayer")
		
	pass # Replace with function body.

# Needs to get record values after save is loaded
func init_records():
	if records.saved_records["1"].size() > 0:
		# Set record with best of world 1
		current_record_value = records.saved_records["1"][0].elapsed_time
		$Record.text = records.get_formatted_elapsed_time(current_record_value)
	else:
		$HBoxContainer/ArrowRight.modulate.a = 0.1

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !active:
		return
		
	# Navigation input
	if Input.is_action_just_pressed("ui_down"):
		options[index].modulate.a = .5
		index += 1
		get_node("/root/TitleScreen/AudioStreamPlayer").play()
		if index >= options.size():
			index = 0
		
		options[index].modulate.a = 1.0
		
	if Input.is_action_just_pressed("ui_up"):
		options[index].modulate.a = .5
		index -= 1
		get_node("/root/TitleScreen/AudioStreamPlayer").play()		
		if index < 0:
			index = options.size() - 1
		options[index].modulate.a = 1.0
			
	
	if Input.is_action_just_pressed("ui_left"):
		move_left()
		
	if Input.is_action_just_pressed("ui_right"):
		move_right()
	# Select input	
	#if Input.is_action_just_pressed("ui_action"):
	#	match index:
	#		0:
	#			# Initiate world
	#			match world:
	#				1:
	#					get_tree().change_scene(scene1)
	#					bgm.change_track(track1)
	#				2:
	#					get_tree().change_scene(scene2)
	#					bgm.change_track(track2)
	#				3:
	#					get_tree().change_scene(scene3)
	#					bgm.change_track(track3)
	#				4:
	#					get_tree().change_scene(scene4)
	#					bgm.change_track(track4)
	#					
	#			stopwatch.start()
	#		1:
	#			# Return to title
	#			animation.play_backwards("title-world")
				
func update_record_text(value):
	$Record.text = records.get_formatted_elapsed_time(value)
	current_record_value = value
	
func move_left():
	if world <= 1:
		world = 1
	else:
		# Check if next world is available, if not, bring arrow up again
		if  world < max_world && records.saved_records[str(world)].size() < 1:
			$Tween.interpolate_property($HBoxContainer/ArrowRight, "modulate",
				Color(1.0,1.0,1.0,0.10), Color(1.0,1.0,1.0,1.0), .4,
				Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		world -= 1
		
		# Update Record text value
		$Tween.interpolate_method(self, "update_record_text", current_record_value, records.saved_records[str(world)][0].elapsed_time, 0.4, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.start()
		get_node("/root/TitleScreen/AudioStreamPlayer2").play()
		match world:
			1:
				animation.play_backwards("world-1-2")
			2:
				animation.play_backwards("world-2-3")
			3:
				animation.play_backwards("world-3-4")

func move_right():
	if world >= max_world:
		world = max_world
	else:
		# Check if next world is available/finished this world
		if  records.saved_records[str(world)].size() > 0:
			world += 1
			get_node("/root/TitleScreen/AudioStreamPlayer2").play()
			# Check if next world is available, if not, put right arrow down
			if  world < max_world && records.saved_records[str(world)].size() < 1:
				$Tween.interpolate_property($HBoxContainer/ArrowRight, "modulate",
					Color(1.0,1.0,1.0,1.0), Color(1.0,1.0,1.0,0.1), .4,
					Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
				$Tween.interpolate_method(self, "update_record_text", current_record_value, 0.0, 0.4, Tween.TRANS_LINEAR, Tween.EASE_IN)
			# Check if current world has records	
			if records.saved_records[str(world)].size() < 1:
				$Tween.interpolate_method(self, "update_record_text", current_record_value, 0.0, 0.4, Tween.TRANS_LINEAR, Tween.EASE_IN)
			else:
				# Update Record text value
				$Tween.interpolate_method(self, "update_record_text", current_record_value, records.saved_records[str(world)][0].elapsed_time, 0.4, Tween.TRANS_LINEAR, Tween.EASE_IN)
	
			$Tween.start()
				
			match world:
				2:
					animation.play("world-1-2")
				3:
					animation.play("world-2-3")
				4:
					animation.play("world-3-4")

func play():
	# Initiate world
	match world:
		1:
			get_tree().change_scene(scene1)
			bgm.change_track(track1)
		2:
			get_tree().change_scene(scene2)
			bgm.change_track(track2)
		3:
			get_tree().change_scene(scene3)
			bgm.change_track(track3)
		4:
			get_tree().change_scene(scene4)
			bgm.change_track(track4)
			
	stopwatch.start()
	
func back():
	# Return to title
	animation.play_backwards("title-world")

func highlight_option(i):
	for child in options:
		child.modulate.a = 0.5
	index = i
	options[index].modulate.a = 1.0
	options[index].grab_focus()
	
func reactivate_menu():
	if active:
		active = false
		options[index].release_focus()
	else:
		active = true
		options[index].grab_focus()


func _on_Start_pressed():
	play()

func _on_Return_pressed():
	back()


func _on_Start_mouse_entered():
	highlight_option(0)

func _on_Return_mouse_entered():
	highlight_option(1)


func _on_ArrowLeft_pressed():
	move_left()
	options[index].grab_focus()


func _on_ArrowRight_pressed():
	move_right()
	options[index].grab_focus()
