extends VBoxContainer

# Flag if menu is open
export (bool) var active = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	# Only process input if menu is active
	if !active:
		return
	
	# Return to title screen
	#if Input.is_action_pressed("ui_action") || Input.is_action_just_pressed("ui_cancel"):
	#	get_node("/root/TitleScreen/AnimationPlayer").play_backwards("title-control")

func back():
	get_node("/root/TitleScreen/AnimationPlayer").play_backwards("title-control")

func reactivate_menu():
	if active:
		active = false
		$Return.release_focus()
	else:
		active = true
		$Return.grab_focus()

func _on_Return_pressed():
	back()


