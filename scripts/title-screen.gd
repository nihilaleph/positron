extends Control

# Animation variables
export (String) var initial_animation = "initial"
var intro = true
export(Resource) var initial_track

# Called when the node enters the scene tree for the first time.
func _ready():
	# Play initial animation
	$AnimationPlayer.play(initial_animation)
	$AnimationPlayer.connect("animation_finished", self, "_on_AnimationPlayer_animation_finished")
	#erase_save()
	load_game()
	
	# Initialize records now that game is loaded
	$CenterContainer/Worlds.init_records()
	
	var bgm
	# Initilize Autoload variables
	bgm = get_node("/root/BGM")
	bgm.stream = initial_track
	bgm.play()
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Code to skip intro. If it's not on intro, ignore
	if !intro:
		return
	if Input.is_action_just_pressed("ui_cancel"):
		$AnimationPlayer.playback_speed = 10.0
	if Input.is_action_just_released("ui_cancel"):
		$AnimationPlayer.playback_speed = 1.0
	
	pass

func _on_AnimationPlayer_animation_finished(anim_name):
	# After intro is finished, reset playback speed
	$AnimationPlayer.disconnect("animation_finished", self, "_on_AnimationPlayer_animation_finished")
	intro = false
	$AnimationPlayer.playback_speed = 1.0
	
func load_game():
	var save_game = File.new()
	if not save_game.file_exists("user://savegame.save"):
		return # Error! We don't have a save to load.

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open("user://savegame.save", File.READ)
	var load_json = JSON.parse(save_game.get_as_text())
	get_node("/root/Records").saved_records = load_json.result
	print(load_json.result)
	save_game.close()
	
func erase_save():
	var dir = Directory.new()
	dir.remove("user://savegame.save")
