extends Control

# Signal to Game controller desire to return
signal return_title

# Menu options variable
var options
var index = 0
var min_index = 0
var max_index
var counter = 0

# Keeps next world files
export(String, FILE, "*.tscn") var next_scene
export(Resource) var next_track

# Called when the node enters the scene tree for the first time.
func _ready():
	# Populate menu options
	options = $NinePatchRect/CenterContainer/VBoxContainer/.get_children()
	# If there is no next world, eliminate first option
	if !next_scene:
		$NinePatchRect/CenterContainer/VBoxContainer/Next.hide()
		options[index].modulate.a = .5
		index = 1
		options[index].modulate.a = 1.0
		min_index = 1
	max_index = options.size()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Paused state
	if !visible:
		return
		
	# Navigation input
	if Input.is_action_just_pressed("ui_down"):
		options[index].modulate.a = .5
		index += 1
		$AudioStreamPlayer.play()
		
		if index >= max_index:
			index = min_index
			
		options[index].modulate.a = 1.0
		
	if Input.is_action_just_pressed("ui_up"):
		options[index].modulate.a = .5
		index -= 1
		$AudioStreamPlayer.play()
		if index < min_index:
			index = max_index - 1
			
		options[index].modulate.a = 1.0
			
	# Select option			
	#if Input.is_action_just_pressed("ui_action"):
	#	# Prevent menu getting activated on previous action
	#	if counter <= 0:
	#		counter += 1
	#	else:
	#		match index:
	#			0:
	#				# Starts new world
	#				get_tree().paused = false
	#				get_tree().change_scene(next_scene)
	#				get_node("/root/BGM").change_track(next_track)
	#				var stopwatch = get_node("/root/Stopwatch")
	#				stopwatch.start()
	#			1:
	#				# Return to title screen
	#				emit_signal("return_title", self)

func update_record(world, new_record):
	var records = get_node("/root/Records")
	var best_record = records.saved_records[str(world)][0]
	$NinePatchRect2/CenterContainer/GridContainer/BestTime.text = records.get_formatted_elapsed_time(best_record["elapsed_time"])
	$NinePatchRect2/CenterContainer/GridContainer/BestName.text = best_record["name"]
	$NinePatchRect2/CenterContainer/GridContainer/CurrentTime.text = records.get_formatted_elapsed_time(new_record["elapsed_time"])
	$NinePatchRect2/CenterContainer/GridContainer/CurrentName.text = new_record["name"]
	if records.saved_records[str(world)].find(new_record) == 0:
		$NinePatchRect2/CenterContainer/GridContainer/AnimationPlayer.play("new-record")

func highlight_option(i):
	for child in options:
		child.modulate.a = 0.5
	index = i
	options[index].modulate.a = 1.0
	options[index].grab_focus()

func next():
	# Starts new world
	get_tree().paused = false
	get_tree().change_scene(next_scene)
	get_node("/root/BGM").change_track(next_track)
	var stopwatch = get_node("/root/Stopwatch")
	stopwatch.start()
	
func back():
	# Return to title screen
	emit_signal("return_title", self)

func _on_RecordMenu_visibility_changed():
	highlight_option(0)


func _on_Next_pressed():
	next()
	options[index].release_focus()


func _on_ReturnToTitle_pressed():
	back()
	options[index].release_focus()


func _on_Next_mouse_entered():
	highlight_option(0)


func _on_ReturnToTitle_mouse_entered():
	highlight_option(1)
