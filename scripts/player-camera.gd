extends Camera2D

# Variables holding current camera target
export(NodePath) var target_path
var target

func _ready():
	target = get_node(target_path)
	pass # Replace with function body.

func set_target(_target):
	target = get_node(_target)

func _process(delta):
	# Follow target with lerp
	if is_instance_valid(target) && target.is_inside_tree():
		global_position = lerp(global_position, target.global_position, 0.1)

# If player enters portal, change target to portal
func _on_Portal_player_entered_portal(portal):
	target = portal
	pass # Replace with function body.
