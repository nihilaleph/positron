extends Area2D

# Enum for agent charge
enum CHARGE { NEGATIVE, POSITIVE }

# Agent charge
export(CHARGE) var charge = CHARGE.POSITIVE
var killed = false
var bodies_inside = []

export var BOUNCE_IMPULSE = 500.0
export var CHARGE_IMPULSE = 5000.0

# Collision Shape initial rotation (for trail particles)
var initial_collision_shape_rotation
var particle_material

# Called when the node enters the scene tree for the first time.
func _ready():
	$Tween.connect("tween_completed", self, "_on_Tween_completed")
	# Set animation according to charge
	match charge:
		CHARGE.POSITIVE:
			$CollisionShape2D/AnimationPlayer.play_backwards("rotation")
		CHARGE.NEGATIVE:
			$CollisionShape2D/AnimationPlayer.play("rotation")
	pass


func _physics_process(delta):
	# Apply impulse to all bodies in aura
	for body in bodies_inside:
		var s = -1 if body.charge == charge else 1
		
		var distance = global_position - body.global_position
		var distance_length = distance.length()
		
		if distance_length < 567:
			var force = s * distance.normalized() * CHARGE_IMPULSE / (distance_length)
		
			body.apply_impulse(Vector2.ZERO, force)
		

func _on_Blackhole_body_entered(body):
	# Kill agents that enter the body
	kill(body)
	pass # Replace with function body.

func kill(body):
	# Death animation inside blackhole body
	$Tween.interpolate_property(body, "scale", body.scale, Vector2(0.1, 0.1), 0.5, Tween.TRANS_EXPO,Tween.EASE_OUT)
	$Tween.interpolate_property(body, "global_position", body.global_position, global_position, 0.5, Tween.TRANS_EXPO,Tween.EASE_OUT)
	$Tween.start()
	
func _on_Tween_completed(object, key):
	object.die()

func _on_Aura_body_entered(body):
	# Add agent that enters aura to list
	if body.get_name() !="TileMap":
		bodies_inside.push_back(body)
	pass # Replace with function body.

func _on_Aura_body_exited(body):
	# Remove agent that enters aura to list
	bodies_inside.erase(body)
	pass # Replace with function body.
