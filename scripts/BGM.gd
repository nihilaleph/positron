extends AudioStreamPlayer

# Tween reference for fade out
onready var tween = get_node("Tween")

export var transition_duration = 1.0
export var transition_type = 1 # TRANS_SINE

var next_track

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
func change_track(track):
	# Save next track reference
	next_track = track
	fade_out()

func fade_out():
	# Tween music volume down to 0
	tween.connect("tween_completed", self, "_on_TweenOut_tween_completed")
	tween.interpolate_property(self, "volume_db", 0, -80, transition_duration, transition_type, Tween.EASE_IN, 0)
	tween.start()
	# When the tween ends, the music will be stopped

func _on_TweenOut_tween_completed(object, key):
	# stop the music -- otherwise it continues to run at silent volume
	tween.disconnect("tween_completed", self, "_on_TweenOut_tween_completed")
	
	stream = next_track
	volume_db = 0
	play()
	#tween.connect("tween_completed", self, "_on_TweenIn_tween_completed")
	#tween.interpolate_property(self, "volume_db", -80, 0, transition_duration, transition_type, Tween.EASE_OUT, 0)
	#tween.start()

func _on_TweenIn_tween_completed(object, key):
	pass # Replace with function body.
