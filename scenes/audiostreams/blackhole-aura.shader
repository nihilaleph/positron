shader_type canvas_item;

vec4 circunference(float radius, float thickness, vec2 uv, vec2 center) {
	float dist = distance(center, uv);
	if (dist > radius && dist < radius + thickness) {
		return vec4(1.0);
	}
	else {
		return vec4(0.0);
	}
}

void fragment() {
	float resolution = TEXTURE_PIXEL_SIZE.x / TEXTURE_PIXEL_SIZE.y;
	
	vec2 center = vec2(0.5);
	float dist = distance(center, UV);
	COLOR = vec4(vec3(1.0), max(0.0, (0.49 + 0.01 * sin(TIME)) - dist));
	COLOR += circunference(0.6 * fract(TIME / 5.0), .002, UV, center) * (0.49 - dist);
	COLOR += circunference((0.98 + 0.005 * sin(TIME)) - dist, .002, UV, center);
	
	
	
}